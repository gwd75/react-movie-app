import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import api from "../../api";
import { ReactCountryFlag } from "react-country-flag";
import moment from "moment";
import imgNotFound from "../../images/img-not-found.png";
import CastPreview from "../../Components/CastPreview/CastPreview";
import MoviePreview from "../../Components/MoviePreview/MoviePreview";
import "./MovieDetails.scss";
import "./RatingColors.scss";

export default function MovieDetails() {
  let slug = useParams();
  const [movieDetails, setMovieDetails] = useState([]);
  const [similarMovies, setSimilarMovies] = useState([]);
  const [casting, setCasting] = useState([]);
  const [videos, setVideos] = useState([]);

  useEffect(() => {
    const fetchMovieDetails = async () => {
      const result = await api.get(
        `https://api.themoviedb.org/3/movie/${slug.slug}?api_key=b736328230c6f476b2e6a233d2735b1c&language=fr-FR`
      );
      setMovieDetails(result.data);
    };

    const fetchSimilarMovies = async () => {
      const result = await api.get(
        `https://api.themoviedb.org/3/movie/${slug.slug}/similar?api_key=b736328230c6f476b2e6a233d2735b1c&language=fr-FR&page=1`
      );
      setSimilarMovies(result.data.results.slice(0, 10));
    };

    const fetchCasting = async () => {
      const result = await api.get(
        `https://api.themoviedb.org/3/movie/${slug.slug}/credits?api_key=b736328230c6f476b2e6a233d2735b1c&language=fr-FR`
      );
      setCasting(result.data.cast.slice(0, 15));
    };

    const fetchVideos = async () => {
      const result = await api.get(
        `https://api.themoviedb.org/3/movie/${slug.slug}/videos?api_key=b736328230c6f476b2e6a233d2735b1c&language=fr-FR`
      );
      setVideos(result.data.results);
    };

    fetchMovieDetails();
    fetchSimilarMovies();
    fetchCasting();
    fetchVideos();
  }, []);

  const durationConvert = (minutes) => {
    var duration = moment.duration(minutes, "minutes");
    var hours = duration._data.hours;
    var minutes =
      duration._data.minutes < 10
        ? "0" + duration._data.minutes
        : duration._data.minutes;
    return hours + "h" + minutes;
  };
  return movieDetails.length === 0 ? (
    ""
  ) : (
    <div className="movie-details-page">
      <div className="backdrop-container">
        <img
          src={movieDetails.backdrop_path === null ? imgNotFound : `https://image.tmdb.org/t/p/original${movieDetails.backdrop_path}`}
          alt=""
          className="backdrop"
        />
      </div>
      <div className="infos">
        <div className="main-infos">
          <div className="poster-container">
            <img
              src={`https://image.tmdb.org/t/p/original${movieDetails.poster_path}`}
              alt=""
              className="poster"
            />
          </div>
          <div className="technical-infos">
            <h2 className="movie-title">{movieDetails.title}</h2>
            <span className="tagline">{movieDetails.tagline}</span>
            <ul className="infos-list">
              <li className="infos-element duration">
                <span className="strong">Durée : </span>
                {durationConvert(movieDetails.runtime)}
              </li>
              <li className="infos-element rating-avg">
                <span className="strong">Note moyenne :</span>
                <div className="progress-bar">
                  <div
                    className={`progress-bar-data w-${
                      movieDetails.vote_average * 10
                    }`}
                  >
                    {movieDetails.vote_average}
                  </div>
                </div>
              </li>
              <li className="infos-element release-date">
                <span className="strong">Date de sortie :</span>{" "}
                {moment(movieDetails.release_date).format("DD/MM/YYYY")}
              </li>
              <li className="infos-element genres">
                <span className="strong">Genres :</span>{" "}
                {movieDetails.genres.map((item) => {
                  return (
                    <span key={item.id} className="genre">
                      <Link to={{ pathname: `/genres/${item.id}` }}>
                        {item.name}
                      </Link>
                    </span>
                  );
                })}
              </li>
              <li className="infos-element origin-country">
                <span className="strong">Pays d'origine :</span>{" "}
                {movieDetails.production_countries.map((item, index) => {
                  return (
                    <span key={index} className="flag">
                      <ReactCountryFlag countryCode={item.iso_3166_1} svg />
                    </span>
                  );
                })}
              </li>
              {movieDetails.homepage === "" ? (
                ""
              ) : (
                <li className="infos-element website">
                  <a
                    href={movieDetails.homepage}
                    className="website-link"
                    target="_blank"
                    rel="noreferrer"
                  >
                    Site Internet
                  </a>
                </li>
              )}
            </ul>
          </div>
        </div>
        <div className="synopsis-content">
          <h3 className="synopsis-title">Synopsis</h3>
          <p className="synopsis">{movieDetails.overview}</p>
        </div>
        <div className="productions-companies-content">
          <h3 className="productions-title">Sociétés de Productions</h3>
          <div className="companies-container">
            {movieDetails.production_companies.map((item) => {
              return (
                <div className="company" key={item.id}>
                  <img
                    src={
                      item.logo_path != null
                        ? `https://image.tmdb.org/t/p/original${item.logo_path}`
                        : imgNotFound
                    }
                    alt={item.name}
                    className="company-img"
                  />
                  <div className="company-infos">
                    <p className="company-name">{item.name}</p>
                    <span className="company-country">
                      <ReactCountryFlag countryCode={item.origin_country} svg />
                    </span>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="casting-content">
          <h3 className="cast-title">Casting</h3>
          <div className="cast-list">
            {casting.map((item, index) => {
              return <CastPreview data={item} key={index} />;
            })}
          </div>
        </div>
        <div className="videos-content">
          <h3 className="videos-title">Liens vidéos</h3>
          <ul className="videos-list">
            {videos.map((item, index) => {
              return item.site === "YouTube" ? (
                <li className="video">
                  <a
                    key={index}
                    href={`https://www.youtube.com/watch?v=${item.key}`}
                    target="_blank"
                    rel="noreferrer"
                  >
                    {item.name}
                  </a>
                </li>
              ) : (
                ""
              );
            })}
          </ul>
        </div>
        <div className="similar-movies-content">
          <h3 className="similar-title">Films Similaires</h3>
          <div className="similar-list">
            {similarMovies.map((item, index) => {
              return <MoviePreview data={item} key={index} />;
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
