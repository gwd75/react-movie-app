import React, { useState, useEffect } from "react";
import api from "../../api";
import MoviePreview from "../../Components/MoviePreview/MoviePreview";
import "./Accueil.scss";

export default function Accueil() {
  const [nowPlaying, setNowPlaying] = useState([]);
  const [popular, setPopular] = useState([]);
  const [topRated, setTopRated] = useState([]);
  const [upcoming, setUpcoming] = useState([]);

  useEffect(() => {
    const fetchNowPlaying = async () => {
      const result = await api.get(
        "https://api.themoviedb.org/3/movie/now_playing?api_key=b736328230c6f476b2e6a233d2735b1c&language=fr-FR&page=1"
      );
      setNowPlaying(result.data.results.slice(0, 15));
    };
    const fetchPopular = async () => {
      const result = await api.get(
        "https://api.themoviedb.org/3/movie/popular?api_key=b736328230c6f476b2e6a233d2735b1c&language=fr-FR&page=1"
      );
      setPopular(result.data.results.slice(0, 15));
    };
    const fetchTopRated = async () => {
      const result = await api.get(
        "https://api.themoviedb.org/3/movie/top_rated?api_key=b736328230c6f476b2e6a233d2735b1c&language=fr-FR&page=1"
      );
      setTopRated(result.data.results);
    };
    const fetchUpcoming = async () => {
      const result = await api.get(
        "https://api.themoviedb.org/3/movie/upcoming?api_key=b736328230c6f476b2e6a233d2735b1c&language=fr-FR&page=1"
      );
      setUpcoming(result.data.results);
    };
    fetchPopular();
    fetchTopRated();
    fetchUpcoming();
    fetchNowPlaying();
  }, []);
  return (
    <div className="welcome-page">
      <div className="movies-content">
        <h3 className="title">Films actuellement au cinéma</h3>
        <div className="movies-container">
          {nowPlaying.map((item, index) => {
            return <MoviePreview data={item} key={index} />;
          })}
        </div>
      </div>
      <div className="movies-content">
        <h3 className="title">Films les plus populaires</h3>
        <div className="movies-container">
          {popular.map((item, index) => {
            return <MoviePreview data={item} key={index} />;
          })}
        </div>
      </div>
      <div className="movies-content">
        <h3 className="title">Films les mieux notés</h3>
        <div className="movies-container">
          {topRated.map((item, index) => {
            return <MoviePreview data={item} key={index} />;
          })}
        </div>
      </div>
      <div className="movies-content">
        <h3 className="title">Films à venir</h3>
        <div className="movies-container">
          {upcoming.map((item, index) => {
            return <MoviePreview data={item} key={index} />;
          })}
        </div>
      </div>
    </div>
  );
}
