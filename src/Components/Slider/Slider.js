import React from 'react'
import Carousel from 'react-multi-carousel'
import MoviePreview from "../MoviePreview/MoviePreview"
import "react-multi-carousel/lib/styles.css"
import './Slider.scss'

export default function Slider({data}) {
    const responsive = {
        superLargeDesktop: {
          // the naming can be any, depends on you.
          breakpoint: { max: 4000, min: 3000 },
          items: 6
        },
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 6
        },
        tablet: {
          breakpoint: { max: 1024, min: 464 },
          items: 2
        },
        mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1
        }
      };
    return (
        <Carousel 
        responsive={responsive}
        infinite={true}
        keyBoardControl={true}
        autoPlay={false}
        customTransition="all .3s"
        >
            {data.map((item) => {
                return (
                    <MoviePreview data={item} />
                )
            })}
        </Carousel>
    )
}
