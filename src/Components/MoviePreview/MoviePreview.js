import React from "react";
import imgNotFound from "../../images/img-not-found.png"
import { Link } from "react-router-dom";
import "./MoviePreview.scss";

export default function MoviePreview({ data }) {
  // console.log(data);
  return (
    <div className="movie-preview">
      <img
        src={data.poster_path === null ? imgNotFound : `https://image.tmdb.org/t/p/original${data.poster_path}`}
        alt=""
      />
      <div className="movie-preview-infos">
        <h3 className="movie-title">{data.title}</h3>
        <Link className="lien" to={{ pathname: `/movie/${data.id}` }}>
          Accéder à la fiche du film
        </Link>
      </div>
    </div>
  );
}
