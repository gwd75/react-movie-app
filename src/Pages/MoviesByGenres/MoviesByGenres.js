import React, { useEffect, useState } from "react";
import { useParams} from "react-router-dom";
import api from "../../api";
import MoviePreview from "../../Components/MoviePreview/MoviePreview";
import "./MoviesByGenres.scss";

export default function MoviesByGenres() {
  let slug = useParams();
  const [movieList, setMovieList] = useState([]);

  useEffect(() => {
    const fetchMovieList = async () => {
      const result = await api.get(
        `https://api.themoviedb.org/3/discover/movie?api_key=b736328230c6f476b2e6a233d2735b1c&with_genres=${slug.slug}&language=fr-FR`
      );
    //   console.log(result.data.results);
      setMovieList(result.data.results);
    };
    fetchMovieList();
  }, []);
  return (
    <div className="movies-genres-page">
      <h1>Films</h1>
      <div className="movies-container">
        {movieList.map((item, index) => {
          return <MoviePreview data={item} key={index} />;
        })}
      </div>
    </div>
  );
}
