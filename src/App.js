import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap";
import Sidebar from "./Components/Sidebar/Sidebar";
import Accueil from "./Pages/Accueil/Accueil";
import Genres from "./Pages/Genres/Genres";
import MovieDetails from "./Pages/MovieDetails/MovieDetails";
import MoviesByGenres from "./Pages/MoviesByGenres/MoviesByGenres";
import PersonDetails from "./Pages/PersonDetails/PersonDetails";
import Recherche from "./Pages/Recherche/Recherche";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router forceRefresh={true}>
      <div className="App">
        <Sidebar />
        <Switch>
          <Route exact path="/" component={Accueil} />
          <Route exact path="/genres" component={Genres} />
          <Route exact path="/genres/:slug" component={MoviesByGenres} />
          <Route exact path="/movie/:slug" component={MovieDetails} />
          <Route exact path="/person/:slug" component={PersonDetails} />
          <Route exact path="/search/:slug" component={Recherche} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
