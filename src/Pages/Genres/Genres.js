import React, { useState, useEffect } from "react";
import api from "../../api";
import { Link } from "react-router-dom";
import "./Genres.scss";

export default function Genres() {
  const [genres, setGenres] = useState([]);

  useEffect(() => {
    const fetchGenres = async () => {
      const result = await api.get(
        "https://api.themoviedb.org/3/genre/movie/list?api_key=b736328230c6f476b2e6a233d2735b1c&language=fr-FR"
      );
      // console.log(result.data.genres);
      setGenres(result.data.genres);
    };
    fetchGenres();
  }, []);
  return (
    <div className="genres-page">
      <h1>Choissisez un genre cinématographique</h1>
      <div className="genres-container">
        {genres.map((item) => {
          return (
            <Link className="lien" to={{ pathname: `/genres/${item.id}` }}>
              <div className="genre">
                <p>{item.name}</p>
              </div>
            </Link>
          );
        })}
      </div>
    </div>
  );
}
