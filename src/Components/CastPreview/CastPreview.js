import React from 'react'
import imgNotFound from '../../images/img-not-found.png'
import { Link } from 'react-router-dom'
import './CastPreview.scss'

export default function CastPreview({data}) {
    return (
        <div className="cast-preview">
            <img src={data.profile_path === null ? imgNotFound : `https://image.tmdb.org/t/p/original${data.profile_path}`} alt="" />
            <div className="cast-preview-infos">
                <h3 className="name">{data.name}</h3>
                <p className="role">{data.character}</p>
                <Link className="lien" to={{pathname: `/person/${data.id}`}}>
                    Accéder à sa fiche
                </Link>
            </div>
        </div>
    )
}
