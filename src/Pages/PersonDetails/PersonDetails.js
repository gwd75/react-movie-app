import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import api from "../../api";
import moment from "moment";
import imgNotFound from "../../images/img-not-found.png";
import MoviePreview from "../../Components/MoviePreview/MoviePreview";
import "./PersonDetails.scss";

export default function PersonDetails() {
  let slug = useParams();
  const [personDetails, setPersonDetails] = useState([]);
  const [filmography, setFilmography] = useState([]);

  useEffect(() => {
    const fetchPersonDetails = async () => {
      const result = await api.get(
        `https://api.themoviedb.org/3/person/${slug.slug}?api_key=b736328230c6f476b2e6a233d2735b1c&language=fr-FR`
      );
      setPersonDetails(result.data);
    };

    const fetchFilmography = async () => {
      const result = await api.get(
        `https://api.themoviedb.org/3/person/${slug.slug}/movie_credits?api_key=b736328230c6f476b2e6a233d2735b1c&language=fr-FR`
      );
      setFilmography(result.data.cast);
    };
    fetchPersonDetails();
    fetchFilmography();
  }, []);

  const difference = (date1 , date2) => {
      return date1.diff(date2,'years');
  }
  return personDetails.length === 0 ?( "" ) : (
    <div className="person-details-page">
      <div className="infos">
        <div className="main-infos">
          <div className="photo-container">
            <img
              src={
                personDetails.profile_path === null
                  ? imgNotFound
                  : `https://image.tmdb.org/t/p/original${personDetails.profile_path}`
              }
              alt=""
            />
          </div>
          <div className="identity-infos">
            <h2 className="name">{personDetails.name}</h2>
            <ul className="infos-list">
              <li className="infos-element birthday">
                <span className="strong">Né(e) le : </span>{moment(personDetails.birthday).format("DD/MM/YYYY")} {personDetails.deathday === null ? ` - ( ${difference(moment(),moment(personDetails.birthday))} ans )` : ""}
              </li>
              <li className="infos-element birth-place">
              <span className="strong">À : </span>{personDetails.place_of_birth}
              </li>
              {personDetails.deathday === null ? (
                ""
              ) : (
                <li className="infos-element deathday">
                  <span className="strong">Décédé(e) le : </span>{moment(personDetails.deathday).format("DD/MM/YYYY")} ( à {difference(moment(personDetails.deathday), moment(personDetails.birthday))} ans )
                </li>
              )}
              {personDetails.homepage === null ? (
                ""
              ) : (
                <li className="infos-element website">
                  <a
                    href={personDetails.homepage}
                    target="_blank"
                    rel="noreferrer"
                  >
                    Site Internet
                  </a>
                </li>
              )}
              {personDetails.imdb_id === null ? (
                ""
              ) : (
                <li className="infos-element imdb">
                  <a
                    href={`https://www.imdb.com/name/${personDetails.imdb_id}`}
                    target="_blank"
                    rel="noreferrer"
                  >
                    Profil IMDB
                  </a>
                </li>
              )}
            </ul>
          </div>
        </div>
        <div className="biography-content">
            <h3>Biography</h3>
            <p className="biography-text">
                {personDetails.biography}
            </p>
        </div>
        <div className="filmography-content">
            <h3>Filmographie</h3>
            <div className="filmography-list">
                {filmography.map((item,index) => {
                    return <MoviePreview data={item} key={index} />
                })}
            </div>
        </div>
      </div>
    </div>
  );
}
