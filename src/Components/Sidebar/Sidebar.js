import React , {useState } from "react";
import { Link } from "react-router-dom";
import TMDBLogo from "../../images/TMDB_Logos/primary.svg";
import Loupe from "../../images/Search.svg"
import "./Sidebar.scss";

export default function Sidebar() {
  const [searchInput, setSearchInput] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
  }

  const handleKeyPress = (e) => {
    setSearchInput(e.target.value);
  }
  return (
    <div className="sidebar">
      <div className="logo-container">
        <a href="https://www.themoviedb.org/" target="_blank" rel="noreferrer">
          <img src={TMDBLogo} alt="Logo de TMDB" />
        </a>
      </div>
      <nav>
        <ul className="nav-list">
          <li className="nav-element">
            <Link className="lien" to="/">
              <p>Accueil</p>
            </Link>
          </li>
          <li className="nav-element">
            <Link className="lien" to="/genres">
              <p>Genres</p>
            </Link>
          </li>
          <li className="nav-element">
            <form action="" className="formSubmit" onSubmit={handleSubmit}>
              <input 
              required
              value={searchInput}
              onChange={(e) => handleKeyPress(e)}
              type="text"
              className="inputRecherche"
              placeholder="Rechercher un film"
              />
              <Link className="lien" to={{pathname: `/search/${searchInput}`}}>
                <button type="submit">
                  <img src={Loupe} alt="icone loupe" className="logoLoupe"/>
                </button>
              </Link>
            </form>
          </li>
        </ul>
      </nav>
    </div>
  );
}
