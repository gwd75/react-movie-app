import React , {useState, useEffect } from 'react';
import api from '../../api';
import {Link, useParams} from "react-router-dom";
import MoviePreview from '../../Components/MoviePreview/MoviePreview';
import './Recherche.scss';

export default function Recherche() {
    let slug = useParams();
    const [movies, setMovies] = useState([])

    useEffect(() => {
        const researchMovies = async () => {
            const result = await api.get(`https://api.themoviedb.org/3/search/movie?api_key=b736328230c6f476b2e6a233d2735b1c&language=fr-FR&query=${slug.slug}&page=1`);
            setMovies(result.data.results)
        }
        researchMovies();
    },[])
    return (
        <div className="research-page">
            <h2>Résultats pour la recherche : {slug.slug}</h2>
            <div className="research-container">
                {movies.map((item,index) => {
                    return <MoviePreview data={item} key={index} />
                })}
            </div>
        </div>
    )
}
